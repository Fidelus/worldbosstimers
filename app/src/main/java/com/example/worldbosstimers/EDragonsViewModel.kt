package com.example.worldbosstimers


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.worldbosstimers.network.WorldBoss
import com.example.worldbosstimers.network.WorldBossApiService
import kotlinx.coroutines.*
import java.time.*
import java.time.temporal.ChronoUnit


private const val BOSS_NAME="EDragons"
class EDragonsViewModel: ViewModel() {


    private val _response = MutableLiveData<String>()
    val response: LiveData<String>
        get() = _response

    private val _worldBoss = MutableLiveData<WorldBoss>()
    val worldBoss: LiveData<WorldBoss>
        get() = _worldBoss


    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.IO)

    private val _timeDifferenceFirst = MutableLiveData<String>()
    val timeDifferenceFirst: LiveData<String>
        get() = _timeDifferenceFirst

    private val _timeDifferenceSecond = MutableLiveData<String>()
    val timeDifferenceSecond: LiveData<String>
        get() = _timeDifferenceSecond

    private val _timeDifferenceThird = MutableLiveData<String>()
    val timeDifferenceThird: LiveData<String>
        get() = _timeDifferenceThird


    init {
        getEDragonsProperties()
    }


    private fun getEDragonsProperties() {
        coroutineScope.launch {
            var getPropertiesDeferred =
                WorldBossApiService.WorldBossApi.retrofitService.getProperties()
            withContext(Dispatchers.Main) {
                var listResult = getPropertiesDeferred.await()
                _response.value = "Success: ${listResult.size} wbosses data retrieved!"
                if (listResult.size > 0) {
                    for (entry in listResult) {
                        if (entry.bossName == BOSS_NAME) {
                            _worldBoss.value = entry
                            break
                        }
                    }
                }
                _timeDifferenceFirst.value = checkTimeSinceKill2(_worldBoss.value!!.lastRespawnEpoch)
                _timeDifferenceSecond.value= checkTimeSinceKill2(_worldBoss.value!!.secondRespawnEpoch)
                _timeDifferenceThird.value= checkTimeSinceKill2(_worldBoss.value!!.thirdRespawnEpoch)
            }
        }
    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}