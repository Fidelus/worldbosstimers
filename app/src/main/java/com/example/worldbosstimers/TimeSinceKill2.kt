package com.example.worldbosstimers



fun checkTimeSinceKill2(respawnEpoch:Long): String {
    val dateNow=System.currentTimeMillis()/1000
    var seconds = dateNow - respawnEpoch

    var minutes =0
    var hours = 0
    var days = 0
    while (seconds >= 60) {
        seconds = seconds - 60
        minutes = minutes + 1
    }

    while (minutes >= 60) {
        minutes = minutes - 60
        hours = hours + 1
    }

    while(hours>=24){
        hours=hours-24
        days=days+1
    }
    return "${days} d ${hours} h ${minutes} m"
}
