package com.example.worldbosstimers

import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

fun checkTimeSinceKill(respawnEpoch:Long): String {
        val current: LocalDateTime = LocalDateTime.now(ZoneOffset.UTC)
        val newdate: LocalDateTime = LocalDateTime.ofEpochSecond(respawnEpoch, 0, ZoneOffset.UTC)
        var minutes = ChronoUnit.MINUTES.between(newdate, current)
        var hours = 0
        var days = 0
        while (minutes >= 60) {
            minutes = minutes - 60
            hours = hours + 1
        }

        while(hours>=24){
            hours=hours-24
            days=days+1
        }
        return "${days} d ${hours} h ${minutes} m"
    }
