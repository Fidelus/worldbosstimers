package com.example.worldbosstimers

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.worldbosstimers.databinding.FragmentVerificationBinding
import kotlinx.android.synthetic.main.fragment_verification.*

class VerificationFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
        val binding:FragmentVerificationBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_verification,container,false)
        binding.confirmButton.setOnClickListener {
            if(binding.editVerification.text.toString() == "goodboi") {
                it.findNavController().navigate(R.id.action_verificationFragment_to_buttonsFragment)
            }
            else{
                Toast.makeText(context,"Wrong keyword, ask Kaiti in discord",Toast.LENGTH_SHORT).show()
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,view!!.findNavController()) || super.onOptionsItemSelected(item)
    }

}