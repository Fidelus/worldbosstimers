package com.example.worldbosstimers

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.worldbosstimers.databinding.FragmentAzuregosBinding
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

/**
 * A simple [Fragment] subclass.
 */
class AzuregosFragment : Fragment() {

    private val viewModel : AzuregosViewModel by lazy{
        ViewModelProviders.of(this).get(AzuregosViewModel::class.java)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val binding : FragmentAzuregosBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_azuregos,container,false)

        binding.setLifecycleOwner(this)
        binding.viewModel=viewModel
        return binding.root
    }
}




