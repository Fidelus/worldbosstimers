package com.example.worldbosstimers

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.worldbosstimers.databinding.FragmentKazzakBinding

/**
 * A simple [Fragment] subclass.
 */
class KazzakFragment : Fragment() {

    private val viewModel : KazzakViewModel by lazy{
        ViewModelProviders.of(this).get(KazzakViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val binding : FragmentKazzakBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_kazzak,container,false)
        binding.setLifecycleOwner(this)
        binding.viewModel=viewModel
        return binding.root
    }

}