package com.example.worldbosstimers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.worldbosstimers.databinding.FragmentEdragonsBinding

class EDragonsFragment : Fragment(){
    private val viewModel : EDragonsViewModel by lazy{
        ViewModelProviders.of(this).get(EDragonsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
        val binding : FragmentEdragonsBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_edragons,container,false)
        binding.setLifecycleOwner(this)
        binding.viewModel=viewModel
        return binding.root
    }
}