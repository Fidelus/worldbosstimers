package com.example.worldbosstimers


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.worldbosstimers.databinding.FragmentButtonsBinding
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_buttons.view.*

/**
 * A simple [Fragment] subclass.
 */
class ButtonsFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val binding: FragmentButtonsBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_buttons, container, false)

        binding.azuregosButton.animate().apply {
            alpha(1.0f)
            setDuration(500)
        }

        binding.kazzakButton.animate().apply {
            alpha(1.0f)
            setDuration(1000)
        }
        binding.edragonsButton.animate().apply {
            alpha(1.0f)
            setDuration(1500)
        }

        binding.azuregosButton.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.action_buttonsFragment_to_azuregosFragment)
        )
        binding.kazzakButton.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.action_buttonsFragment_to_kazzakFragment)
        )

        binding.edragonsButton.setOnClickListener (
            Navigation.createNavigateOnClickListener(R.id.action_buttonsFragment_to_EDragonsFragment)
        )
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu,menu )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,view!!.findNavController()) || super.onOptionsItemSelected(item)
    }

}
