package com.example.worldbosstimers

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.worldbosstimers.databinding.FragmentReportSpawnBinding
import java.util.*

class ReportSpawnFragment :Fragment(), DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener {

    var day = 0
    var month = 0
    var year = 0
    var hour = 0
    var minute = 0

    var savedDay = 0
    var savedMonth = 0


    var savedYear = 0
    var savedHour = 0
    var savedMinute = 0
    private lateinit var binding:FragmentReportSpawnBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_report_spawn,container,false)
        binding.setLifecycleOwner(this)
        binding.addSpawnTimerButton.setOnClickListener {
            getDateTimeCalendar()
            DatePickerDialog(this.context!!,this,year,month,day).show()
        }

        return binding.root
    }

    private fun getDateTimeCalendar(){
        val cal:Calendar = Calendar.getInstance()
        day=cal.get(Calendar.DAY_OF_MONTH)
        month=cal.get(Calendar.MONTH)
        year=cal.get(Calendar.YEAR)
        hour=cal.get(Calendar.HOUR)
        minute=cal.get(Calendar.MINUTE)
    }


    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDay=dayOfMonth
        savedMonth=month
        savedYear=year
        getDateTimeCalendar()
        TimePickerDialog(this.context!!,this,hour,minute, true).show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        savedHour = hourOfDay
        savedMinute = minute
        binding.datePickedTextView.text="$savedYear-$savedMonth-$savedDay $savedHour:$savedMinute"
    }
}