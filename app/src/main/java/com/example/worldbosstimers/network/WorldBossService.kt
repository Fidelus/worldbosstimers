package com.example.worldbosstimers.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

private const val BASE_URL="https://ethel.pl/wow/"
private val moshi=Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

interface WorldBossApiService {
    @GET("data.json")
    fun getProperties():
            Deferred<List<WorldBoss>>

    object WorldBossApi{
        val retrofitService : WorldBossApiService by lazy {
            retrofit.create(WorldBossApiService::class.java)
        }
    }
}
