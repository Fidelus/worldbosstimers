package com.example.worldbosstimers.network

data class WorldBoss (
    val bossName : String,
    val lastRespawn : String,
    val lastRespawnEpoch :Long,
    val lastRespawnLayer : String,
    val secondRespawn : String,
    val secondRespawnEpoch :Long,
    val secondRespawnLayer : String,
    val thirdRespawn : String,
    val thirdRespawnEpoch :Long,
    val thirdRespawnLayer : String,
    val entries : Int


)
